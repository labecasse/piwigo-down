const dotenv = require('dotenv')
const path = require('path')

dotenv.config()

module.exports = {
  'HOST': process.env.HOST,
  'USERNAME': process.env.USERNAME,
  'PASSWORD': process.env.PASSWORD,
  'ROOT': path.isAbsolute(process.env.ROOT) ? process.env.ROOT : path.join(__dirname, process.env.ROOT),
  'CATEGORY': parseInt(process.env.CATEGORY),
  'ONLY_FAVORITE': process.env.FAVORITE
}
