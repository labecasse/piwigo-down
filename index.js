const PiwigoApi = require('../piwigo-api')
const fs = require('fs')
const path = require('path')
const http = require('https')
const config = require('./config.js')
const client = new PiwigoApi(config.HOST)

client.sessionLogin(config.USERNAME, config.PASSWORD, (res) => {
  client.getCategoriesList(config.CATEGORY, (err, res) => {
    if (err) throw err
    const categoryName = res.result.categories[0].name
    const categoryPath = path.join(config.ROOT, categoryName)
    if (!fs.existsSync(categoryPath)) {
      fs.mkdirSync(categoryPath)
    }
    cloneCategories(config.CATEGORY, categoryPath)
  })
})

/*
 * Download all images of a given category
 * including sub categories
 */
function cloneCategories (parentCat, dir) {
  return new Promise((resolve, reject) => {
    client.getCategoriesList(parentCat, (er, res) => {
      if (er) return console.log(parentCat, er)

      let promises = []
      for (let cat of res.result.categories) {
        if (parentCat !== cat.id) {
          let catDir = path.join(dir, cat.name)
          if (!fs.existsSync(catDir)) {
            console.log(cat.name)
            fs.mkdirSync(catDir)
          }

          promises.push(cloneCategories(cat.id, catDir))
        }
      }

      return Promise.all(promises)
        .then(downloadImages(parentCat, dir))
        .then(() => resolve())
    })
  })
}

/*
 * Download all images of the category
 */
function downloadImages (cat, dir) {
  return new Promise(function (resolve, reject) {
    client.getCategoriesImages(cat, (err, res) => {
      if (err) return reject(err)

      const promises = []
      for (let image of res.result.images) {
        if (image.derivatives.xxlarge) {
          let file = path.join(dir, image.file)

          if (!fs.existsSync(file)) {
            let stream = fs.createWriteStream(file)
            let url = image.derivatives.xxlarge.url

            if (path.extname(file) === '.mp4' ||
                path.extname(file) === '.webm') {
              url = image.element_url
            }

            let p = new Promise((resolve, reject) => {
              http.get(url, (res) => {
                res.pipe(stream)
                stream.on('finish', () => stream.close(resolve))
              })
            })
            promises.push(p)
          }
        }
      }

      Promise.all(promises)
        .then(() => resolve())
    })
  })
}

function downloadImage (id, dir) {
  return new Promise(function(resolve, reject) {
    client.getImagesInfo(id, (err, res) => {
      if (err) return reject(err)
      let image = res.result
    })
  })
}
